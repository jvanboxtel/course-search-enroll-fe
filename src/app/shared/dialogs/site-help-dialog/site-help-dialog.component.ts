import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Inject, Component } from '@angular/core';

@Component({
  selector: 'cse-site-help-dialog',
  styles: [
    '.site-header-dialog { padding:24px!important; }' +
      '.mat-dialog-content { min-height: 250px; }' +
      '.mat-dialog-actions { justify-content: flex-end; }',
  ],
  template:
    '<div class="site-header-dialog"><h1 mat-dialog-title>Need more Help?</h1>' +
    '<div mat-dialog-content>' +
    '<mat-list>' +
    '<mat-list-item>' +
    '<a target="_blank" color="primary" href="/help">Support</a>' +
    '</mat-list-item>' +
    '<mat-list-item>' +
    '<a target="_blank" color="primary" href="https://kb.wisc.edu/helpdesk/search.php?q=&cat=8136">FAQs</a>' +
    '</mat-list-item>' +
    '<mat-list-item>' +
    '<a target="_blank" color="primary" href="https://kb.wisc.edu/enrollment/page.php?id=86702">Recent Changes</a> ' +
    '</mat-list-item>' +
    '</mat-list>' +
    '</div>' +
    '<div mat-dialog-actions>' +
    '<button mat-button mat-stroked-button (click)="onNoClick()">Ok</button>' +
    '</div></div>',
})
export class SiteHelpDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<SiteHelpDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: object,
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
