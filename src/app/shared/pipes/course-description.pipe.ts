import { Pipe, PipeTransform } from '@angular/core';
import { ConstantsService } from '@app/degree-planner/services/constants.service';

@Pipe({ name: 'courseDescription', pure: true })
export class CourseDescriptionPipe implements PipeTransform {
  constructor(private constants: ConstantsService) {}

  transform(arg: { subjectCode: string; catalogNumber: number | string }) {
    const { short } = this.constants.subjectDescription(arg.subjectCode);
    return `${short} ${arg.catalogNumber}`;
  }
}
