import { Component, OnDestroy, ViewChild, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSidenav } from '@angular/material/sidenav';
import { Profile } from '@app/core/models/profile';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from '@angular/forms';
import { Course } from '@app/core/models/course';
import {
  WebsocketService,
  TokenPayload,
} from './shared/services/websocket.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'cse-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [WebsocketService],
})
export class AppComponent implements OnInit, OnDestroy {
  coursesData$: any;
  selectedDegreePlan: number;
  courses: Course[];
  profile: Profile;
  coursesForm: FormGroup;
  subjectCode: FormControl;
  @ViewChild('rightAddCourse', { static: false })
  public rightAddCourse: MatSidenav;

  connected = false;
  connectionState = 'Disconnected';
  lastMessage: string;
  jwtToken = '';
  subscription: Subscription;
  messageSubscription: Subscription;

  constructor(
    public dialog: MatDialog,
    private fb: FormBuilder,
    private wsclient: WebsocketService,
  ) {
    this.subjectCode = new FormControl('', [Validators.required]);
    this.coursesForm = this.fb.group({
      coursesInput: null,
    });
  }

  ngOnInit() {
    this.subscription = this.wsclient.connectionState.subscribe(message => {
      this.connectionState = message;
      if (this.connectionState === 'Disconnected!') {
        this.wsclient.ws.reconnect();
      }
    });
    this.messageSubscription = this.wsclient.messages.subscribe(message => {
      this.lastMessage = message;
    });
    this.getToken();
  }

  getToken() {
    this.wsclient.getToken().subscribe((token: TokenPayload) => {
      this.jwtToken = token.token;
      this.connect();
    });
  }
  connect() {
    const token = this.jwtToken;
    this.wsclient.connectToServer(token);
    this.connected = true;
    this.connectionState = 'Connected';
  }
  disconnect() {
    this.wsclient.disconnect();
    this.connected = false;
    this.connectionState = 'Disconnected';
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.messageSubscription.unsubscribe();
  }
}
