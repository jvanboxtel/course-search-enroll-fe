import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent as DegreePlannerHeaderComponent } from './degree-planner/header/header.component';
import { JumpLinksComponent as DegreePlannerJumpLinksComponent } from './degree-planner/jump-links/jump-links.component';
import { JumpLinksComponent as DarsJumpLinksComponents } from './dars/jump-links/jump-links.component';

const routes: Routes = [
  {
    path: 'dars',
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./dars/dars.module').then(m => m.DARSModule),
        outlet: 'primary',
      },
      {
        path: '',
        component: DarsJumpLinksComponents,
        outlet: 'jump-links',
      },
    ],
  },
  {
    path: '',
    pathMatch: 'full',
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./degree-planner/degree-planner.module').then(
            m => m.DegreePlannerModule,
          ),
        outlet: 'primary',
      },
      {
        path: '',
        component: DegreePlannerJumpLinksComponent,
        outlet: 'jump-links',
      },
      {
        path: '',
        component: DegreePlannerHeaderComponent,
        outlet: 'header-buttons',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [],
})
export class AppRoutingModule {}
